import { Component, Input, OnInit } from '@angular/core';
import { Movie } from '../movie';
import { MovieService } from '../movie.service';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() movie:Movie;
  

  constructor(public moviService: MovieService) { }

  ngOnInit(): void {
    
  }

  getMovieImg(){
    return this.moviService.getMovieImg(500, this.movie.backdrop_path)
  }

  getMovieGenre(){
    const res = this.moviService.getMovieGenres(this.movie.genre_ids)
    return res ? res : [];
  }
}
