export interface Movie{
    id: number,
    adult: boolean,
    backdrop_path: string,
    genre_ids: number[],
    original_language: string,
    original_title: string,
    overview: string,
    popularity: number,
    poster_path: string,
    release_date: string,
    title: string,
    video: boolean,
    vote_average: number,
    vote_count: number
}

export interface MoviDetails{
    adult: boolean,
    backdrop_path: string,
    belongs_to_collection: any,
    budget: number,
    genres: Object[],
    homepage: string,
    id: number,
    imdb_id: string,
    original_language: string,
    original_title: string,
    overview: string,
    popularity:number,
    poster_path: string,
    production_companies: Object[],
    production_countries: Object[],
    release_date: string,
    revenue: number,
    runtime: number,
    spoken_languages: Object[],
    status: string,
    tagline: string,
    title: string,
    video: boolean,
    vote_average: number,
    vote_count: number

}

export interface MovieData{
    page: number,
    results: Movie[],
    total_pages: number,
    total_results: number
}

export interface Config{
    change_keys: string[],
    images: any
}

export interface Genre{
    id: number,
    name: string
}
export interface GenreData {
    genres: Genre[]
}