import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Movie } from '../movie';
import { MovieService } from '../movie.service';
import { SlickCarouselComponent } from 'ngx-slick-carousel';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit, OnChanges, OnDestroy {

  @Input() id: number;
  recommended:Movie[];
  serviceSubscription : Subscription;
  
  constructor(
    private movieService: MovieService
  ) { }

  ngOnChanges(changes: SimpleChanges){
    this.getRecommendations();
  }

  ngOnInit(): void {
    this.getRecommendations();
  }
  
  
  getRecommendations():void{
  this.serviceSubscription = this.movieService.getRecommendedMovies(this.id)
      .subscribe(data => this.recommended = data.results)
  }
  
  @ViewChild('slickModal') slickModal: SlickCarouselComponent;
  
  next() {
    this.slickModal.slickNext();
  }


  prev() {
    this.slickModal.slickPrev();
  }
  
  slideConfig = {"slidesToShow": 4, "slidesToScroll": 1};

  ngOnDestroy(){
    this.serviceSubscription.unsubscribe();
  }
  
}


