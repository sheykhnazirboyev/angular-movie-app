import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { MOVIES } from '../mock-movies';
import {Movie, MovieData} from '../movie';
import {MovieService} from '../movie.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit, OnDestroy {
  serviceSubscription: Subscription;
  loading:boolean = false;
  constructor(
    private movieService: MovieService
  ) { }

  ngOnInit(): void {
    this.getMovies();
  }


  movies:Movie[];
  movieData: MovieData;

  getMovies(page?:number):void{
    window.scroll(0, 0);
    this.loading = true;
    this.serviceSubscription =  this.movieService.getMovies(page)
      .subscribe(movies => {
        this.movies = movies.results;
        this.movieData = movies;
        this.loading = false;
      })
  }

  updatePage($event: any){
    this.getMovies($event) 
  }

  ngOnDestroy() {
    this.serviceSubscription.unsubscribe();
  }

}
