import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MovieData } from '../movie';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})

export class PaginationComponent implements OnInit {
  @Input() page;
  @Input() total;
  @Output() updatePage = new EventEmitter();

  constructor(private movieService: MovieService) { }

  ngOnInit(): void {
    
  }

  setPage(num: number):void{
    this.updatePage.emit(num)
  }

}
