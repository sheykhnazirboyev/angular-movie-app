import { Injectable } from '@angular/core';
import {Movie, Config, MovieData, MoviDetails, Genre, GenreData} from './movie';
import {Observable, of} from 'rxjs';
import {catchError, map, tap, debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {MOVIES} from './mock-movies';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class MovieService {

  constructor(private http: HttpClient) { 

  }

  protected api_key = "9c6833fb32ed6cc1bfabc58b5ff28a9c";

  base_img_url:string;
  base_URL="https://api.themoviedb.org/3/";
  movieData:MovieData;
  config:Config;
  genres:Genre[];

  getConfig():void{
    const url = `${this.base_URL}configuration?api_key=${this.api_key}`;

    this.http.get<Config>(url).subscribe((res) => {
      this.config = res;
      this.base_img_url = res.images.secure_base_url;
    });
  }

  getGenres():void{
    const url = `${this.base_URL}genre/movie/list?api_key=${this.api_key}`;
    
    this.http.get<GenreData>(url)
    .pipe(
      catchError(this.handleError<GenreData>("get all genres", {} as GenreData ))
    )
      .subscribe(data => {
        this.genres = data.genres ? data.genres : [];
      });
  }

  

  getMovies(page?:number):Observable<MovieData>{
    
    const  url = `${this.base_URL}movie/popular?api_key=${this.api_key}&page=${page}`
    
    return this.http.get<MovieData>(url)
      .pipe(
        catchError(this.handleError<MovieData>("get movies", {} as MovieData))
      )
  }
                                                                                                                                                                                                                                                                                                                                    
  getMovie(id: number):Observable<MoviDetails>{
    const url = `${this.base_URL}movie/${id}?api_key=${this.api_key}`

    return this.http.get<MoviDetails>(url).pipe(
      catchError(this.handleError<MoviDetails>("catch movie", {} as MoviDetails))
    )
  }

  getMovieImg(width, backdrop_path){
    if(!this.base_img_url){
        return "assets/loading_img.png"
    }
    return `${this.base_img_url}w${width}${backdrop_path}`
  }

  getMovieGenres(gnreIds: number[]){
    let result
    if(this.genres){
      result =  gnreIds.map(id => this.genres.find(g => g.id === id));
    } else {
      result = [];
    }
      
    
    return result;
  }

  getRecommendedMovies(id:number):Observable<MovieData>{
    const url = `${this.base_URL}movie/${id}/recommendations?api_key=${this.api_key}`;

    return this.http.get<MovieData>(url)
  }

  searchMovies(term: string):Observable<MovieData>{
    
    if(!term.trim()){
      return of({} as MovieData);
    }

    const url = `${this.base_URL}search/movie?api_key=${this.api_key}&query=${term}`;
    return this.http.get<MovieData>(url);
  }
  
  private handleError<T>(operation = "operation", result?:T){
    return (error:any):Observable<T> => {
      console.log(`operation failed error=${error.message}`)
      return of(result);
    }
  }

}
