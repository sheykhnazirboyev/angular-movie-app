import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MoviDetails, Movie } from '../movie';
import {ActivatedRoute} from '@angular/router';
import { MovieService } from '../movie.service';
import { Observable, Subscription } from 'rxjs';
import { query } from '@angular/animations';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit, OnDestroy {
  serviceSubscription: Subscription;
  movie:MoviDetails; 
  loading:boolean = false; 

  constructor(
    private route: ActivatedRoute,
    private movieService: MovieService
  ) { }

  ngOnInit(): void {
    window.scroll(0, 0);
    this.getMovie();
  }
  
  getMovie():void{
     this.route.params.subscribe(queryParams => {
      window.scroll(0, 0);
      this.loading = true;

      this.serviceSubscription = this.movieService.getMovie(queryParams.id)
        .subscribe(m => {
          this.movie = m;
          this.loading = false;
        });  
    })
  }

  getMovieImg(){
    const res = this.movieService.getMovieImg(342, this.movie.poster_path)
    
    return res;
  }

  renderStars(){
    return Array.from({length: this.movie.vote_average});
  }

  ngOnDestroy()  {
    this.serviceSubscription.unsubscribe();
  }

}
