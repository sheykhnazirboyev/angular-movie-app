import { Component, OnInit } from '@angular/core';
import { MovieService } from './movie.service';
import {slideInAnimation} from './animations';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [ slideInAnimation ]
})
export class AppComponent implements OnInit{
  title = 'movie-app';
  parentValue = "parentValue"

  constructor(
    private movieService: MovieService
  ){}

  getAnimationData(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

  ngOnInit(){
    this.movieService.getConfig();
    this.movieService.getGenres();
  }

}
