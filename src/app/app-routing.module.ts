import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import { MoviesComponent } from './movies/movies.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { NotfoundComponent } from './notfound/notfound.component';

const routes:Routes = [
  {path: "", redirectTo:"movies", pathMatch:"full"},
  {path: "movies", component: MoviesComponent, data: {animation: "movies"}},
  { path: "movies/:id", component: MovieDetailComponent, data: {animation: "movie"} },
  {path: "**", component: NotfoundComponent}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
