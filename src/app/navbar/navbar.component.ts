import { Component, OnInit, Input, OnDestroy} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Movie, MovieData } from '../movie';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy{
  @Input() value:string = "initial value";

  movies$:Observable<MovieData>
  movies:Movie[]
  private searchTerms = new Subject<string>();

  constructor(private movieService: MovieService) { }
  
  search(term:string):void{
    this.searchTerms.next(term);
  }

  closeList(){
    this.searchTerms.next("");
  }

  ngOnInit(): void {
        this.movies$ = this.searchTerms.pipe(
          debounceTime(300),
          distinctUntilChanged(),
          switchMap((term: string) => {
            return this.movieService.searchMovies(term)
          })
        )
        this.movies$.subscribe(res => this.movies = res.results)
        
  }

  ngOnDestroy(){
    this.searchTerms.unsubscribe();
  }


}
